"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getUser = exports.createUser = void 0;

var _user = _interopRequireDefault(require("../models/user.model"));

var _error = require("../commons/error");

var _response = require("../commons/response");

var _auth = require("../services/auth.service");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var createUser = function createUser(req, res, next) {
  var newUser;
  return regeneratorRuntime.async(function createUser$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.prev = 0;
          newUser = new _user["default"](req.body);
          _context.next = 4;
          return regeneratorRuntime.awrap(newUser.save());

        case 4:
          newUser = _context.sent;
          new _response.CustomResponse({
            statusCode: 201,
            result: newUser.transform()
          })["return"](res);
          _context.next = 11;
          break;

        case 8:
          _context.prev = 8;
          _context.t0 = _context["catch"](0);
          next(_context.t0);

        case 11:
        case "end":
          return _context.stop();
      }
    }
  }, null, null, [[0, 8]]);
};

exports.createUser = createUser;

var getUser = function getUser(req, res, next) {
  var user;
  return regeneratorRuntime.async(function getUser$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.prev = 0;
          _context2.next = 3;
          return regeneratorRuntime.awrap(_user["default"].findOne({
            email: req.user.email
          }));

        case 3:
          user = _context2.sent;
          new _response.CustomResponse({
            result: user.transform(),
            statusCode: 200
          })["return"](res);
          _context2.next = 10;
          break;

        case 7:
          _context2.prev = 7;
          _context2.t0 = _context2["catch"](0);
          next(_context2.t0);

        case 10:
        case "end":
          return _context2.stop();
      }
    }
  }, null, null, [[0, 7]]);
};

exports.getUser = getUser;