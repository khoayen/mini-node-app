"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.login = void 0;

var _response = require("../commons/response");

var _error = require("../commons/error");

var _auth = require("../services/auth.service");

var login = function login(req, res, next) {
  return regeneratorRuntime.async(function login$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          if (!(req.body.password === req.user.password)) {
            _context.next = 2;
            break;
          }

          return _context.abrupt("return", new _response.CustomResponse({
            statusCode: 200,
            result: {
              user: req.user.transform(),
              token: (0, _auth.createToken)(req.user.email, req.user.username)
            }
          })["return"](res));

        case 2:
          next(new _error.CustomError({
            statusCode: 400,
            error: "Wrong password"
          }));

        case 3:
        case "end":
          return _context.stop();
      }
    }
  });
};

exports.login = login;