"use strict";

require("@babel/polyfill");

var _express = _interopRequireDefault(require("express"));

var _morgan = _interopRequireDefault(require("morgan"));

var _bodyParser = _interopRequireDefault(require("body-parser"));

var _database = _interopRequireDefault(require("./services/database.service"));

var _routers = _interopRequireDefault(require("./routers"));

var _error = require("./commons/error");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

require("dotenv").config();

var app = (0, _express["default"])();
var PORT = process.env.PORT || 3000;
app.use(_bodyParser["default"].urlencoded({
  extended: false
}));
app.use((0, _morgan["default"])("dev"));
(0, _database["default"])();
app.use(_routers["default"]);
app.use(_error.notFoundError, _error.errHandler);
app.listen(PORT, function (err) {
  console.log(err || "App is listening on port ".concat(PORT));
});
module.exports = app;