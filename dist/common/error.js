"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.handleError = exports.CustomError = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var CustomError = function CustomError(_ref) {
  var error = _ref.error;

  _classCallCheck(this, CustomError);

  this.error = error;
};

exports.CustomError = CustomError;

var handleError = function handleError(err) {
  console.log(err);
};

exports.handleError = handleError;