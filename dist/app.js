"use strict";

var _error = require("./common/error");

var args = require('yargs').argv;

var log = require("./common/logger");

var add = require("./controller/add");

var read = require("./controller/read");

var list = require("./controller/list");

var remove = require("./controller/remove");

var job = process.argv[2];
var argsLen = Object.keys(args).length;

try {
  var attr = Object.entries(args).slice(1, argsLen - 1);
  attr = Object.fromEntries(attr);

  if (job === "list") {
    list();
  } else {
    if (attr.length === 0) throw new _error.CustomError({
      error: "No argment found"
    });

    if (!attr.title) {
      throw new _error.CustomError({
        error: "No title found in command"
      });
    }

    switch (job) {
      case "add":
        add(attr);
        break;

      case "read":
        read(attr.title);
        break;

      case "remove":
        remove(attr.title);
        break;

      default:
        throw new _error.CustomError({
          error: "No action found"
        });
    }
  }
} catch (e) {
  (0, _error.handleError)(e);
}