"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var options = {
  useNewUrlParser: true,
  autoIndex: true,
  useCreateIndex: true,
  useFindAndModify: false,
  useUnifiedTopology: true
};
var DB_URI = process.env.DB_URI || "mongodb+srv://khoayen:khoamalab_B1@cluster0-yvppm.mongodb.net/test";

var connect = function connect() {
  _mongoose["default"].connect(DB_URI, options, function (error) {
    console.log(error || "Database connected");
  });
};

var _default = connect;
exports["default"] = _default;