"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.checkAuth = exports.createToken = void 0;

var _jsonwebtoken = _interopRequireDefault(require("jsonwebtoken"));

var _error = require("../commons/error");

var _user = _interopRequireDefault(require("../models/user.model"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var createToken = function createToken(email, username) {
  try {
    var token = _jsonwebtoken["default"].sign({
      email: email,
      username: username
    }, "secret");

    return token;
  } catch (error) {
    throw error;
  }
};

exports.createToken = createToken;

var checkAuth = function checkAuth(req, res, next) {
  var token, _jwt$verify, email, username, user, invalidTokenError;

  return regeneratorRuntime.async(function checkAuth$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.prev = 0;
          token = req.headers.authorization;
          _jwt$verify = _jsonwebtoken["default"].verify(token, "secret"), email = _jwt$verify.email, username = _jwt$verify.username;
          _context.next = 5;
          return regeneratorRuntime.awrap(_user["default"].findOne({
            username: username
          }));

        case 5:
          user = _context.sent;
          req.user = user;
          next();
          _context.next = 14;
          break;

        case 10:
          _context.prev = 10;
          _context.t0 = _context["catch"](0);
          invalidTokenError = new _error.CustomError({
            error: "Invalid token",
            statusCode: 401
          });
          next(invalidTokenError);

        case 14:
        case "end":
          return _context.stop();
      }
    }
  }, null, null, [[0, 10]]);
};

exports.checkAuth = checkAuth;