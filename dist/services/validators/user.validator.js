"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createUserValidate = exports.createUserSchema = void 0;

var _joi = _interopRequireDefault(require("@hapi/joi"));

var _validator = require("../../commons/validator");

var _error = require("../../commons/error");

var _user = _interopRequireDefault(require("../../models/user.model"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var createUserSchema = _joi["default"].object({
  username: _joi["default"].string().alphanum().min(5).required(),
  fullName: _joi["default"].string(),
  email: _joi["default"].string().email({
    minDomainSegments: 2
  }).required(),
  password: _joi["default"].string().min(6).required()
});

exports.createUserSchema = createUserSchema;

var createUserValidate = function createUserValidate(req, res, next) {
  var error, validationError, email, foundEmail, username, foundUsername, newError;
  return regeneratorRuntime.async(function createUserValidate$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.prev = 0;
          error = [];
          validationError = (0, _validator.getValidationError)(createUserSchema, req.body);

          if (!validationError) {
            _context.next = 5;
            break;
          }

          throw validationError.error;

        case 5:
          email = req.body.email;
          _context.next = 8;
          return regeneratorRuntime.awrap(_user["default"].findOne({
            email: email
          }));

        case 8:
          foundEmail = _context.sent;

          if (foundEmail) {
            error.push("Email existed");
          }

          username = req.body.username;
          _context.next = 13;
          return regeneratorRuntime.awrap(_user["default"].findOne({
            username: username
          }));

        case 13:
          foundUsername = _context.sent;

          if (foundUsername) {
            error.push("Username existed");
          }

          if (!(error.length > 0)) {
            _context.next = 17;
            break;
          }

          throw error;

        case 17:
          return _context.abrupt("return", next());

        case 20:
          _context.prev = 20;
          _context.t0 = _context["catch"](0);
          newError = new _error.CustomError({
            statusCode: 412,
            error: _context.t0
          });
          next(newError);

        case 24:
        case "end":
          return _context.stop();
      }
    }
  }, null, null, [[0, 20]]);
};

exports.createUserValidate = createUserValidate;