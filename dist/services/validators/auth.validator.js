"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.validateLoginForm = void 0;

var _user = _interopRequireDefault(require("../../models/user.model"));

var _joi = _interopRequireDefault(require("@hapi/joi"));

var _validator = require("../../commons/validator");

var _error = require("../../commons/error");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var loginSchema = _joi["default"].object({
  username: _joi["default"].string().required(),
  password: _joi["default"].string().required()
});

var validateLoginForm = function validateLoginForm(req, res, next) {
  var validationError, foundUser, newError;
  return regeneratorRuntime.async(function validateLoginForm$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.prev = 0;
          validationError = (0, _validator.getValidationError)(loginSchema, req.body);

          if (!validationError) {
            _context.next = 4;
            break;
          }

          throw validationError;

        case 4:
          _context.next = 6;
          return regeneratorRuntime.awrap(_user["default"].findOne({
            username: req.body.username
          }));

        case 6:
          foundUser = _context.sent;

          if (foundUser) {
            _context.next = 9;
            break;
          }

          throw "User not found";

        case 9:
          req.user = foundUser;
          return _context.abrupt("return", next());

        case 13:
          _context.prev = 13;
          _context.t0 = _context["catch"](0);
          newError = new _error.CustomError({
            statusCode: 412,
            error: _context.t0
          });
          next(newError);

        case 17:
        case "end":
          return _context.stop();
      }
    }
  }, null, null, [[0, 13]]);
};

exports.validateLoginForm = validateLoginForm;