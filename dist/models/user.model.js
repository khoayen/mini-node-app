"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mongoose = require("mongoose");

var userSchema = new _mongoose.Schema({
  username: {
    type: String,
    required: true
  },
  fullName: {
    type: String,
    trim: true
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  }
});

userSchema.methods.transform = function () {
  return {
    id: this._id,
    fullName: this.fullName,
    email: this.email,
    username: this.username
  };
};

var _default = (0, _mongoose.model)("USER", userSchema);

exports["default"] = _default;