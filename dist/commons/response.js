"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CustomResponse = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var CustomResponse =
/*#__PURE__*/
function () {
  function CustomResponse(_ref) {
    var result = _ref.result,
        statusCode = _ref.statusCode;

    _classCallCheck(this, CustomResponse);

    this.result = result;
    this.statusCode = statusCode;
  }

  _createClass(CustomResponse, [{
    key: "return",
    value: function _return(res) {
      res.status(this.statusCode).send(this.result);
    }
  }]);

  return CustomResponse;
}();

exports.CustomResponse = CustomResponse;