"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.errHandler = exports.notFoundError = exports.CustomError = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var CustomError = function CustomError(_ref) {
  var error = _ref.error,
      statusCode = _ref.statusCode;

  _classCallCheck(this, CustomError);

  this.error = error;
  this.statusCode = statusCode;
};

exports.CustomError = CustomError;

var notFoundError = function notFoundError(req, res, next) {
  throw new CustomError({
    error: "Not found",
    statusCode: 404
  });
};

exports.notFoundError = notFoundError;

var errHandler = function errHandler(err, req, res, next) {
  console.log(err);

  if (err instanceof CustomError) {
    return res.status(err.statusCode).json(err);
  }

  res.status(500).json(new CustomError({
    statusCode: 500,
    error: "Server error"
  }));
};

exports.errHandler = errHandler;