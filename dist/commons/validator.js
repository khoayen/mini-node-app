"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getValidationError = void 0;

var _error = require("./error");

var options = {
  abortEarly: false
};

var getValidationError = function getValidationError(schema, data) {
  var _schema$validate = schema.validate(data, options),
      error = _schema$validate.error;

  if (error) {
    var errorList = error.details.map(function (error) {
      return error.message.replace(/\[/g, "").replace(/\]/g, "") // eslint-disable-next-line no-useless-escape
      .replace(/\"/g, "'").replace(/child /g, "");
    });
    return new _error.CustomError({
      statusCode: 412,
      error: errorList
    });
  }

  return null;
}; // export const validationErrorHandle = (error) => {
//     if (error) {
//         return next(error)
//     }
//     else return next()
// }


exports.getValidationError = getValidationError;