"use strict";

var _error = require("../common/error");

var fs = require('fs');

var readFile = require("./readFile");

module.exports = function (newNote) {
  try {
    var notes = readFile();
    notes.push(newNote);
    fs.writeFileSync("notes.json", JSON.stringify(notes));
    return 1;
  } catch (error) {
    return 0;
  }
};