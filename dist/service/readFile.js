"use strict";

var _error = require("../common/error");

var _util = require("util");

var fs = require('fs');

module.exports = function () {
  try {
    if (!fs.existsSync("notes.json")) {
      var createStream = fs.createWriteStream("notes.json");
      createStream.end();
    }

    var contents = fs.readFileSync("notes.json", 'utf8');

    if (!contents.length) {
      return contents = [];
    }

    contents = JSON.parse(contents);
    return contents;
  } catch (error) {
    throw new _error.CustomError({
      error: "Reading error"
    });
  }
};