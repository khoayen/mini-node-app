"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _user = _interopRequireDefault(require("../models/user.model"));

var _user2 = require("../services/validators/user.validator");

var _user3 = require("../controllers/user.controller");

var _auth = require("../services/auth.service");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var router = (0, _express.Router)();
router.get("/", _auth.checkAuth, _user3.getUser);
router.post("/", _user2.createUserValidate, _user3.createUser);
var _default = router;
exports["default"] = _default;