"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _user = _interopRequireDefault(require("./user.router"));

var _express = require("express");

var _auth = require("../controllers/auth.controller");

var _auth2 = require("../services/validators/auth.validator");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var router = (0, _express.Router)();
router.get("/", function (req, res, next) {
  res.json({
    message: "HELLO WORLD"
  });
});
router.use("/user", _user["default"]);
router.post("/login", _auth2.validateLoginForm, _auth.login);
var _default = router;
exports["default"] = _default;