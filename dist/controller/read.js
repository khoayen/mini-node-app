"use strict";

var find = require("../controller/find");

var logMessage = require("../common/logger");

var getNotes = require("../service/readFile");

module.exports = function (title) {
  var foundNote = find(title);

  if (!foundNote) {
    return logMessage("Not found", "error");
  }

  logMessage("".concat(foundNote.title), "info");
  var values = Object.values(foundNote);
  values.forEach(function (value, index) {
    if (index !== 0) {
      console.log(value);
    }
  });
};