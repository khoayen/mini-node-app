"use strict";

var find = require("../controller/find");

var read = require("../service/readFile");

var write = require("../service/writeFile");

var logMessage = require("../common/logger");

module.exports = function (note) {
  var foundNote = find(note.title);

  if (foundNote) {
    return logMessage("Note title taken", "error");
  }

  var successWriting = write(note);

  if (successWriting === 1) {
    return logMessage("New note added", "success");
  }

  return logMessage("Error", "error");
};