"use strict";

var _error = require("../common/error");

var fs = require('fs');

var find = require("../controller/find");

var read = require("../service/readFile");

var write = require("../service/writeFile");

var logMessage = require("../common/logger");

module.exports = function (title) {
  var foundNote = find(title);

  if (!foundNote) {
    return logMessage("Not found", "error");
  }

  var notes = read();
  notes.splice(notes.indexOf(foundNote), 1);

  try {
    fs.writeFileSync("notes.json", JSON.stringify(notes));
    return logMessage("Note removed", "success");
  } catch (e) {
    (0, _error.handleError)(e);
  }
};