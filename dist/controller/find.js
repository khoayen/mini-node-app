"use strict";

var getNotes = require("../service/readFile");

module.exports = function (title) {
  var notes = getNotes();
  var foundNote = notes.find(function (note) {
    return note.title === title;
  });

  if (!foundNote) {
    return 0;
  }

  return foundNote;
};