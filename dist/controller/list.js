"use strict";

var read = require("../service/readFile");

var logMessage = require("../common/logger");

var getNotes = require("../service/readFile");

module.exports = function () {
  var notes = getNotes();
  if (!notes.length) return;
  logMessage("Your notes", "info");
  notes.forEach(function (note, index) {
    var msg = "".concat(index + 1, ". ");
    note = Object.entries(note);
    note.forEach(function (element, i) {
      if (i === 0) msg += "".concat(element[0], ": ").concat(element[1]);else msg += " - ".concat(element[0], ": ").concat(element[1]);
    });
    console.log(msg);
  });
};