const read = require("../service/readFile")
const logMessage = require("../common/logger")
const getNotes = require("../service/readFile")
module.exports = function () {
    const notes = getNotes();
    if (!notes.length)
        return
    logMessage("Your notes", "info")
    notes.forEach((note, index) => {
        let msg = `${index + 1}. `;
        note = Object.entries(note);

        note.forEach((element, i) => {
            if (i === 0)
                msg += `${element[0]}: ${element[1]}`
            else msg += ` - ${element[0]}: ${element[1]}`
        });

        console.log(msg)
    });
}