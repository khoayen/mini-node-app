const find = require("../controller/find")
const logMessage = require("../common/logger")
const getNotes = require("../service/readFile")
module.exports = function (title) {
    const foundNote = find(title)
    if (!foundNote) {
        return logMessage("Not found", "error")
    }
    logMessage(`${foundNote.title}`, "info")
    const values = Object.values(foundNote)
    values.forEach((value, index) => {
        if(index !== 0){
            console.log(value)
        }
    });
}