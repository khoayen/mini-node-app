import { handleError } from '../common/error';

const fs = require('fs');
const find = require("../controller/find")
const read = require("../service/readFile")
const write = require("../service/writeFile")
const logMessage = require("../common/logger")
module.exports = function (title) {
    const foundNote = find(title)
    if (!foundNote) {
        return logMessage("Not found", "error")
    }
    const notes = read();
    notes.splice(notes.indexOf(foundNote), 1)
    try {
        fs.writeFileSync("notes.json", JSON.stringify(notes))
        return logMessage("Note removed", "success")
    } catch (e) {
        handleError(e)
    }
}