const getNotes = require("../service/readFile")

module.exports = function (title) {
    const notes = getNotes();
    const foundNote = notes.find(function (note) {
        return note.title === title
    })
    if (!foundNote) {
        return 0
    }
    
    return foundNote;
}