const find = require("../controller/find")
const read = require("../service/readFile")
const write = require("../service/writeFile")
const logMessage = require("../common/logger")
module.exports = function (note) {
    const foundNote = find(note.title)
    if (foundNote) {
        return logMessage("Note title taken", "error")
    }

    const successWriting = write(note);

    if (successWriting === 1) {
        return logMessage("New note added", "success")
    }
    return logMessage("Error", "error")
}