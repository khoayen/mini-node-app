import { errHandler, handleError, CustomError } from './common/error';

const args = require('yargs').argv;
const log = require("./common/logger")
const add = require("./controller/add")
const read = require("./controller/read")
const list = require("./controller/list")
const remove = require("./controller/remove")

const job = process.argv[2]

const argsLen = Object.keys(args).length
try {
    let attr = Object.entries(args).slice(1, argsLen - 1)
    attr = Object.fromEntries(attr)

    if (job === "list") {
        list()
    }
    else {

        if (attr.length === 0)
            throw new CustomError({ error: "No argment found" })
        if (!attr.title) {
            throw new CustomError({ error: "No title found in command" })
        }
        switch (job) {
            case "add":
                add(attr)
                break
            case "read":
                read(attr.title)
                break
            case "remove":
                remove(attr.title)
                break
            default: throw new CustomError({ error: "No action found" })
        }
    }
}
catch (e) {
    handleError(e)
}