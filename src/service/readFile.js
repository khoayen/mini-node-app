import { handleError, CustomError } from '../common/error';
import { isArray } from 'util';

const fs = require('fs');

module.exports = function () {
    try {
        if (!fs.existsSync("notes.json")) {
            const createStream = fs.createWriteStream("notes.json");
            createStream.end();
        }
        var contents = fs.readFileSync("notes.json", 'utf8');
        if (!contents.length) {
            return contents = []
        }
        contents = JSON.parse(contents);
        return contents

    } catch (error) {
        throw new CustomError({ error: "Reading error" })
    }
}