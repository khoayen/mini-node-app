const fs = require('fs');
import { handleError } from '../common/error';
const readFile = require("./readFile")

module.exports = function (newNote) {
    try {
        let notes = readFile();
        notes.push(newNote)
        fs.writeFileSync("notes.json", JSON.stringify(notes));
        return 1

    } catch (error) {
        return 0
    }
}