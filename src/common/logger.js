import { CustomError } from './error';


var colors = require('colors');
colors.setTheme({
    info: 'bgWhite',
    success: 'bgGreen',
    error: 'bgRed'
});

module.exports = function (data, type) {
    switch (type) {
        case "success":
            console.log(colors.black(data).success)
            break;
        case "error":
            console.log(colors.black(data).error)
            break;
        case "info":
            console.log(colors.black(data).info)
            break;
        default: throw new CustomError({error: "Wrong type of logger choosen"})
    }
}
